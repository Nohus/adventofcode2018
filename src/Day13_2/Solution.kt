package Day13_2

import Day13_2.Direction.*
import Day13_2.TrackType.*
import solveInputs
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

enum class Direction {
    UP, RIGHT, DOWN, LEFT
}

enum class TrackType {
    HORIZONTAL, VERTICAL, SLASH, BACKSLASH, JUNCTION, NONE
}

enum class Turn {
    LEFT, STRAIGHT, RIGHT;

    fun next(): Turn {
        return when (this) {
            LEFT -> STRAIGHT
            STRAIGHT -> RIGHT
            RIGHT -> LEFT
        }
    }

}

data class Cart(var direction: Direction, var nextTurn: Turn = Turn.LEFT)

data class Track(val type: TrackType, var cart: Cart? = null) {
    override fun toString(): String {
        if (cart != null) {
            return when (cart!!.direction) {
                UP -> "^"
                RIGHT -> ">"
                DOWN -> "v"
                LEFT -> "<"
            }
        } else {
            return when (type) {
                HORIZONTAL -> "-"
                VERTICAL -> "|"
                SLASH -> "/"
                BACKSLASH -> "\\"
                JUNCTION -> "+"
                NONE -> " "
            }
        }
    }
}

fun printMap(map: Array<Array<Track>>) {
    map.forEachIndexed { y, column ->
        println(column.joinToString(""))
    }
}

fun countCarts(map: Array<Array<Track>>): Int {
    return map.sumBy { it.sumBy { if (it.cart != null) 1 else 0 } }
}

fun findCart(map: Array<Array<Track>>): Pair<Int, Int> {
    map.forEachIndexed { y, column ->
        column.forEachIndexed { x, track ->
            if (track.cart != null) {
                return Pair(x, y)
            }
        }
    }
    throw Exception("No carts")
}

fun BufferedImage.drawTrack(track: Track, x: Int, y: Int, trackUp: Boolean, explosion: Boolean) {
    val backgroundColor = Color(255,255,255).rgb
    val trackColor = Color(120,144,156).rgb
    val cartColor = Color(33,33,33).rgb
    val cartDirectionColor = Color(183,28,28).rgb
    val explosionColor = Color(244,67,54).rgb
    for (cx in x..(x + 2)) {
        for (cy in y..(y + 2)) {
            setRGB(cx, cy, backgroundColor)
        }
    }
    when (track.type) {
        HORIZONTAL -> {
            setRGB(x, y + 1, trackColor)
            setRGB(x + 1, y + 1, trackColor)
            setRGB(x + 2, y + 1, trackColor)
        }
        VERTICAL -> {
            setRGB(x + 1, y, trackColor)
            setRGB(x + 1, y + 1, trackColor)
            setRGB(x + 1, y + 2, trackColor)
        }
        SLASH -> {
            if (trackUp) {
                setRGB(x + 1, y, trackColor)
                setRGB(x, y + 1, trackColor)
            } else {
                setRGB(x + 2, y + 1, trackColor)
                setRGB(x + 1, y + 2, trackColor)
            }
        }
        BACKSLASH -> {
            if (trackUp) {
                setRGB(x + 1, y, trackColor)
                setRGB(x + 2, y + 1, trackColor)
            } else {
                setRGB(x + 1, y + 2, trackColor)
                setRGB(x, y + 1, trackColor)
            }
        }
        JUNCTION -> {
            setRGB(x, y + 1, trackColor)
            setRGB(x + 1, y + 1, trackColor)
            setRGB(x + 2, y + 1, trackColor)
            setRGB(x + 1, y, trackColor)
            setRGB(x + 1, y + 2, trackColor)
        }
        else -> {}
    }
    if (explosion) {
        for (ex in 0..2) {
            for (ey in 0..2) {
                setRGB(x + ex, y + ey, explosionColor)
            }
        }
    }
    track.cart?.let { cart ->
        setRGB(x, y, cartColor)
        setRGB(x, y + 1, cartColor)
        setRGB(x, y + 2, cartColor)
        setRGB(x + 1, y, cartColor)
        setRGB(x + 1, y + 1, cartColor)
        setRGB(x + 1, y + 2, cartColor)
        setRGB(x + 2, y, cartColor)
        setRGB(x + 2, y + 1, cartColor)
        setRGB(x + 2, y + 2, cartColor)
        when (cart.direction) {
            UP -> {
                setRGB(x, y, cartDirectionColor)
                setRGB(x + 1, y, cartDirectionColor)
                setRGB(x + 2, y, cartDirectionColor)
            }
            RIGHT -> {
                setRGB(x + 2, y, cartDirectionColor)
                setRGB(x + 2, y + 1, cartDirectionColor)
                setRGB(x + 2, y + 2, cartDirectionColor)
            }
            DOWN -> {
                setRGB(x, y + 2, cartDirectionColor)
                setRGB(x + 1, y + 2, cartDirectionColor)
                setRGB(x + 2, y + 2, cartDirectionColor)
            }
            LEFT -> {
                setRGB(x, y, cartDirectionColor)
                setRGB(x, y + 1, cartDirectionColor)
                setRGB(x, y + 2, cartDirectionColor)
            }
        }
    }
}

fun saveVideoFrame(image: BufferedImage, map: Array<Array<Track>>, width: Int, height: Int, time: Int, collisions: List<Pair<Int, Int>>) {
    for (y in 0 until height) {
        for (x in 0 until width) {
            val trackUp = map.getOrNull(y - 1)?.get(x)?.type in listOf(VERTICAL, JUNCTION)
            val explosion = Pair(x, y) in collisions
            image.drawTrack(map[y][x], x * 3, y * 3, trackUp, explosion)
        }
    }
    ImageIO.write(image, "png", File("tracks/${String.format("%05d", time)}.png"))
}

fun main() {
    solveInputs { input ->
        val height = 150
        val width = 150
        var map = Array(height) { Array(width) { Track(NONE) } }
        val collisions = mutableListOf<Pair<Int, Int>>()
//        val image = BufferedImage(width * 3, height * 3, BufferedImage.TYPE_INT_RGB)
        input.lines().forEachIndexed { y, line ->
            line.chunked(1).forEachIndexed { x, char ->
                val track = when (char) {
                    "/" -> Track(SLASH)
                    "\\" -> Track(BACKSLASH)
                    "-" -> Track(HORIZONTAL)
                    "|" -> Track(VERTICAL)
                    "<" -> Track(HORIZONTAL, Cart(LEFT))
                    ">" -> Track(HORIZONTAL, Cart(RIGHT))
                    "^" -> Track(VERTICAL, Cart(UP))
                    "v" -> Track(VERTICAL, Cart(DOWN))
                    "+" -> Track(JUNCTION)
                    " " -> Track(NONE)
                    else -> throw Exception(char)
                }
                map[y][x] = track
            }
        }
        var time = 0
        var lastCart = ""
        while (true) {
            time++
            val newMap = Array(height) { Array(width) { Track(NONE) } }
            for (y in 0 until height) {
                for (x in 0 until width) {
                    newMap[y][x] = Track(map[y][x].type)
                }
            }
            for (y in 0 until height) {
                for (x in 0 until width) {
                    map[y][x].cart?.let { cart ->
                        val direction = cart.direction
                        val target = when (direction) {
                            UP -> Pair(x, y - 1)
                            DOWN -> Pair(x, y + 1)
                            LEFT -> Pair(x - 1, y)
                            RIGHT -> Pair(x + 1, y)
                        }
                        val track = map[target.second][target.first]
                        val newDirection = when {
                            track.type == SLASH && direction == UP -> RIGHT
                            track.type == SLASH && direction == DOWN -> LEFT
                            track.type == SLASH && direction == RIGHT -> UP
                            track.type == SLASH && direction == LEFT -> DOWN
                            track.type == BACKSLASH && direction == UP -> LEFT
                            track.type == BACKSLASH && direction == DOWN -> RIGHT
                            track.type == BACKSLASH && direction == RIGHT -> DOWN
                            track.type == BACKSLASH && direction == LEFT -> UP
                            track.type == JUNCTION -> {
                                when (direction) {
                                    UP -> {
                                        when (cart.nextTurn) {
                                            Turn.LEFT -> LEFT
                                            Turn.STRAIGHT -> UP
                                            Turn.RIGHT -> RIGHT
                                        }
                                    }
                                    RIGHT -> {
                                        when (cart.nextTurn) {
                                            Turn.LEFT -> UP
                                            Turn.STRAIGHT -> RIGHT
                                            Turn.RIGHT -> DOWN
                                        }
                                    }
                                    DOWN -> {
                                        when (cart.nextTurn) {
                                            Turn.LEFT -> RIGHT
                                            Turn.STRAIGHT -> DOWN
                                            Turn.RIGHT -> LEFT
                                        }
                                    }
                                    LEFT -> {
                                        when (cart.nextTurn) {
                                            Turn.LEFT -> DOWN
                                            Turn.STRAIGHT -> LEFT
                                            Turn.RIGHT -> UP
                                        }
                                    }
                                }.also { cart.nextTurn = cart.nextTurn.next() }
                            }
                            else -> direction
                        }
                        cart.direction = newDirection
                        if (newMap[target.second][target.first].cart != null || map[target.second][target.first].cart != null) {
                            newMap[target.second][target.first].cart = null
                            map[target.second][target.first].cart = null
                            collisions += target
                        } else {
                            newMap[target.second][target.first].cart = cart
                            map[y][x].cart = null
                        }
                    }
                }
            }
            map = newMap
//            saveVideoFrame(image, map, width, height, time, collisions)
//            printMap(map)
            if (countCarts(map) == 1) {
                val last = findCart(map)
                lastCart = "${last.first},${last.second}"
//                for (extraFrame in 1..240) {
//                    saveVideoFrame(image, map, width, height, time + extraFrame, collisions)
//                }
                break
            }
        }
        lastCart
    }
}
