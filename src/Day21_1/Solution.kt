package Day21_1

fun main() {
    println(program())
}

fun program(): Int {
    var c = 65536
    var d = 14070682
    while (true) {
        d += (c and 255)
        d = d and 16777215
        d *= 65899
        d = d and 16777215
        if (256 > c) {
            return d
        } else {
            c /= 256
        }
    }
}
