package Day11_2

import solveInputs

fun powerLevel(x: Int, y: Int, serial: Int): Int {
    val rackId = x + 10
    return ((((rackId * y) + serial) * rackId).toString().reversed().getOrNull(2)?.toString()?.toInt() ?: 0) - 5
}

fun main() {
    solveInputs { input ->
        val serial = input.toInt()
        var max = 0
        var bestX = 0
        var bestY = 0
        var bestSize = 0
        var noChangeCount = 0
        for (size in 1..300) {
            var solutionUpdated = false
            for (x in 1..(300 - (size - 1))) {
                for (y in 1..(300 - (size - 1))) {
                    var sum = 0
                    for (squareX in x..(x + (size - 1))) {
                        for (squareY in y..(y + (size - 1))) {
                            sum += powerLevel(squareX, squareY, serial)
                        }
                    }
                    if (sum > max) {
                        max = sum
                        bestX = x
                        bestY = y
                        bestSize = size
                        solutionUpdated = true
                    }
                }
            }
            println("Size $size, best: [$bestX,$bestY,$bestSize] at $max")
            if (!solutionUpdated) {
                noChangeCount++
                if (noChangeCount >= 10) {
                    break
                }
            } else {
                noChangeCount = 0
            }
        }
        "$bestX,$bestY,$bestSize"
    }
}
