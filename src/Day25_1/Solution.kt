package Day25_1

import solveInputs
import kotlin.math.abs

data class Point(val coordinates: List<Int>) {

    fun distanceTo(other: Point): Int {
        return coordinates.zip(other.coordinates).map { abs(it.first - it.second) }.sum()
    }

}

typealias Constellation = List<Point>

fun Point.isNearby(constellation: Constellation): Boolean {
    return constellation.any { distanceTo(it) <= 3 }
}

fun main() {
    solveInputs { input ->
        val points = input.lines().map { Point(it.split(",").map(String::toInt)) }
        val constellations = mutableListOf<Constellation>()
        points.forEach { point ->
            val joinedConstellations = constellations.filter { point.isNearby(it) }
            constellations += if (joinedConstellations.isEmpty()) {
                mutableListOf(point)
            } else {
                val mergedConstellation = joinedConstellations.reduce { acc, constellation -> acc + constellation } + point
                constellations.removeAll(joinedConstellations)
                mergedConstellation
            }
        }
        constellations.size
    }
}
