package Day6_1

import solveInputs
import kotlin.math.abs

fun dist(x: Int, y: Int, x1: Int, y1: Int): Int {
    return abs(x - x1) + abs(y - y1)
}

fun main() {
    solveInputs { input ->
        val coordinates = input.lines().map { it.split(", ")[0].toInt() to it.split(", ")[1].toInt() }
        val width = coordinates.map { it.first }.max()!! + 10
        val height = coordinates.map { it.second }.max()!! + 10
        val map = mutableMapOf<Pair<Int, Int>, Pair<Int, Int>>()
        val ignore = mutableSetOf<Pair<Int, Int>>()
        for (x in 0..width) {
            for (y in 0..height) {
                val distances = coordinates.map { it to dist(it.first, it.second, x, y) }.sortedBy { it.second }
                if (distances[0].second != distances[1].second) {
                    if (x == 0 || x == width || y == 0 || y == height) {
                        ignore += distances[0].first
                    } else {
                        map[x to y] = distances[0].first
                    }
                }
            }
        }
        val biggest = map.entries.groupBy { it.value }.map { it.key to it.value.size }
            .sortedByDescending { it.second }.first { it.first !in ignore }.second
        "$biggest"
    }
}
