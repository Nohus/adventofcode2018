package Day15_2

import solveInputs

var height = 0
var width = 0
var map = mutableMapOf<Pos, Field>()

typealias Board = Map<Pos, Field>
data class Pos(val x: Int, val y: Int)
abstract class Field
class Wall : Field() {
    override fun toString(): String {
        return "#"
    }
}
class Floor : Field() {
    override fun toString(): String {
        return "."
    }
}
class Mob(val type: Char, var health: Int = 200) : Field() {
    override fun toString(): String {
        return "$type"
    }
}

fun Board.loop(action: (Pos, Field) -> Unit) {
    for (y in 0 until height) {
        for (x in 0 until width) {
            action(Pos(x, y), this[Pos(x, y)]!!)
        }
    }
}

fun Board.print() {
    for (y in 0 until height) {
        for (x in 0 until width) {
            print(this[Pos(x, y)])
        }
        println("")
    }
}

fun Board.getMobs(): List<Pos> {
    val matches = mutableListOf<Pos>()
    loop { pos, field ->
        if (field is Mob) {
            matches += pos
        }
    }
    return matches
}

fun Pos.getNeighbors(): List<Pos> {
    return listOf(Pos(x, y - 1), Pos(x - 1, y), Pos(x + 1, y), Pos(x, y + 1))
}

fun Pos.isFloor(): Boolean {
    return map[this] is Floor
}

fun List<Pos>.readingOrder(): List<Pos> {
    return this.sortedWith(compareBy({ it.y }, { it.x }))
}

class Distance(val distance: Int, val previous: Pos? = null)

private fun dijkstra(from: Pos, to: Pos): List<Pos>? {
    val visited = mutableSetOf<Pos>()
    val unvisited = mutableListOf<Pos>()
    val distances = mutableMapOf<Pos, Distance>()
    distances[from] = Distance(0)
    var current = from
    while (true) {
        unvisited += current.getNeighbors().filter { it.isFloor() }.filter { it !in unvisited }.filter { it !in visited }
        unvisited.forEach { pos ->
            val distance = distances[current]!!.distance + 1
            if (distance < (distances[pos]?.distance ?: Int.MAX_VALUE)) {
                distances[pos] = Distance(distance, current)
            }
        }
        unvisited -= current
        visited += current
        if (current == to) {
            val route = mutableListOf(to)
            var distance = distances[to]!!
            while (distance.previous != null) {
                route.add(distance.previous!!)
                distance = distances[distance.previous!!]!!
            }
            route.reverse()
            return route.drop(1)
        }
        current = unvisited.sortedBy { distances[it]!!.distance }.firstOrNull() ?: break
    }
    return null
}

fun simulate(elfAttack: Int): Int? {
    var rounds = 0
    while (true) {
        for (unitPos in map.getMobs()) {
            if (map[unitPos] is Mob) {
                val unit = map[unitPos]!! as Mob
                val enemyType = if (unit.type == 'E') 'G' else 'E'
                val enemies = map.getMobs().filter { (map[it] as Mob).type == enemyType }
                if (enemies.isEmpty()) {
                    val totalHealth = map.getMobs().map { (map[it] as Mob).health }.sum()
                    return rounds * totalHealth
                }
                var immediateEnemies = unitPos.getNeighbors().filter { it in enemies }
                var newPos = unitPos
                if (immediateEnemies.isEmpty()) { // Move
                    val candidatePositions = enemies.map { it.getNeighbors().filter { it.isFloor() } }.flatten()
                    val rangedPositions = candidatePositions.map { it to (dijkstra(unitPos, it)?.size ?: Int.MAX_VALUE) }
                        .filter { it.second < Int.MAX_VALUE }.sortedBy { it.second }
                    if (rangedPositions.isNotEmpty()) {
                        val minDistance = rangedPositions.minBy { it.second }!!.second
                        val selected = rangedPositions.filter { it.second == minDistance }.map { it.first }.readingOrder().first()
                        val step = dijkstra(unitPos, selected)!!.first()
                        map[unitPos] = Floor()
                        map[step] = unit
                        newPos = step
                    }
                }
                immediateEnemies = newPos.getNeighbors().filter { it in enemies }
                if (immediateEnemies.isNotEmpty()) { // Fight
                    val victimPos = immediateEnemies.readingOrder().sortedBy { (map[it] as Mob).health }.first()
                    val enemy = map[victimPos] as Mob
                    enemy.health -= if (unit.type == 'E') elfAttack else 3
                    if (enemy.health <= 0) {
                        map[victimPos] = Floor()
                        if (enemy.type == 'E') {
                            return null
                        }
                    }
                }
            }
        }
        rounds++
    }
}

fun Board.copy(): MutableMap<Pos, Field> {
    val newBoard = mutableMapOf<Pos, Field>()
    loop { pos, field ->
        newBoard[pos] = when (field) {
            is Mob -> Mob(field.type, 200)
            else -> field
        }
    }
    return newBoard
}

fun main() {
    solveInputs { input ->
        height = input.lines().size
        width = input.lines().first().length
        input.lines().forEachIndexed { y, line ->
            line.forEachIndexed { x, char ->
                map[Pos(x, y)] = when (char) {
                    '#' -> Wall()
                    '.' -> Floor()
                    'E' -> Mob('E')
                    'G' -> Mob('G')
                    else -> throw Exception("Invalid input: $char")
                }
            }
        }
        val startingMap = map.copy()

        var elfAttack = 4
        while (true) {
            println("Trying with elf attack $elfAttack")
            val outcome = simulate(elfAttack)
            if (outcome != null) {
                return@solveInputs outcome.toString()
            }
            map = startingMap.copy()
            elfAttack++
        }
        ""
    }
}
