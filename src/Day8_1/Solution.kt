package Day8_1

import solveInputs

fun readNode(list: MutableList<Int>): Int {
    val nodes = list.removeAt(0)
    val entries = list.removeAt(0)
    var sum = 0
    repeat(nodes) {
        sum += readNode(list)
    }
    repeat(entries) {
        sum += list.removeAt(0)
    }
    return sum
}

fun main() {
    solveInputs { input ->
        val list = input.split(" ").map { it.toInt() }.toMutableList()
        "${readNode(list)}"
    }
}
