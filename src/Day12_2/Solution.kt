package Day12_2

import solveInputs

fun printPots(pots: List<Boolean>) {
    println(pots.map { if (it) '#' else '.' }.joinToString(""))
}

fun isShifted(pots: List<Boolean>, newPots: List<Boolean>): Boolean {
    if (pots.size != newPots.size) return false
    val shiftedPots = (listOf(false) + pots).dropLast(1)
    for (i in 0..pots.lastIndex) {
        if (shiftedPots[i] != newPots[i]) {
            return false
        }
    }
    return true
}

data class Rule(val pattern: List<Boolean>, val result: Boolean)

fun main() {
    solveInputs { input ->
        var pots = input.lines().first().substringAfter(": ").map { it == '#' }
        val rules = input.lines().drop(1).map {
            val split = it.split(" => ")
            val pattern = split[0].map { it == '#' }
            Rule(pattern, split[1] == "#")
        }
        var leftPad = 0
        val targetGeneration = 50_000_000_000
        for (generation in 0 until targetGeneration) {
            if (pots.take(3).any { it }) {
                pots = (List(10) { false } + pots).toMutableList()
                leftPad += 10
            }
            if (pots.takeLast(3).any { it }) {
                pots = (pots + List(10) { false }).toMutableList()
            }
            val newPots = MutableList(pots.size) { false }
            for (index in 0..(pots.lastIndex - 4)) {
                for (rule in rules) {
                    var matches = true
                    for (i in 0..4) {
                        val plant = pots.getOrElse(index + i) { false }
                        if (plant != rule.pattern[i]) {
                            matches = false
                            break
                        }
                    }
                    if (matches) {
                        newPots[index + 2] = rule.result
                        break
                    }
                }
            }
            if (isShifted(pots, newPots)) {
                val remainingGenerations = targetGeneration - generation
                var sum = 0L
                pots.forEachIndexed { index, plant ->
                    if (plant) {
                        sum += index - leftPad + remainingGenerations
                    }
                }
                return@solveInputs "$sum"
            }
            pots = newPots
        }
        ""
    }
}
