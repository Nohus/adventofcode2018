package Day16_2

import solveInputs

fun parseRegisters(text: String): List<Int> {
    return text
        .substringAfter("[")
        .substringBefore("]")
        .split(", ")
        .map(String::toInt)
}

fun executeOpcode(opcode: String, arguments: List<Int>, inputRegisters: List<Int>): List<Int> {
    val registers = inputRegisters.toMutableList()
    val a = arguments[0]
    val b = arguments[1]
    val c = arguments[2]
    registers[c] = when (opcode) {
        "addr" -> registers[a] + registers[b]
        "addi" -> registers[a] + b
        "mulr" -> registers[a] * registers[b]
        "muli" -> registers[a] * b
        "banr" -> registers[a] and registers[b]
        "bani" -> registers[a] and b
        "borr" -> registers[a] or registers[b]
        "bori" -> registers[a] or b
        "setr" -> registers[a]
        "seti" -> a
        "gtir" -> if (a > registers[b]) 1 else 0
        "gtri" -> if (registers[a] > b) 1 else 0
        "gtrr" -> if (registers[a] > registers[b]) 1 else 0
        "eqir" -> if (a == registers[b]) 1 else 0
        "eqri" -> if (registers[a] == b) 1 else 0
        "eqrr" -> if (registers[a] == registers[b]) 1 else 0
        else -> throw Exception("Invalid opcode")
    }
    return registers
}

fun main() {
    solveInputs { input ->
        val examples = input.substringBeforeLast("]") + "]"
        val program = input.substringAfterLast("]").trim()
        val opcodes = listOf("addr", "addi", "mulr", "muli", "banr", "bani", "borr", "bori", "setr", "seti", "gtir", "gtri", "gtrr", "eqir", "eqri", "eqrr")
        val mappings = mutableMapOf<Int, MutableList<String>>()
        examples.lines().chunked(3).forEach {
            val beforeRegisters = parseRegisters(it[0])
            val afterRegisters = parseRegisters(it[2])
            val instruction = it[1].split(" ").map(String::toInt)
            val possibleOpcodes = opcodes.filter { executeOpcode(it, instruction.drop(1), beforeRegisters) == afterRegisters }
            mappings[instruction[0]]?.let { it.removeAll { it !in possibleOpcodes } } ?: run { mappings[instruction[0]] = possibleOpcodes.toMutableList() }
        }
        while (mappings.values.any { it.size > 1 }) {
            mappings.toMutableMap().filterValues { it.size == 1 }.values.flatten().forEach { known ->
                mappings.mapValues { entry -> if (entry.value.size > 1) entry.value.removeAll { it in known } }
            }
        }
        var registers = listOf(0, 0, 0, 0)
        program.lines().map { it.split(" ").map(String::toInt) }.forEach { instruction ->
            registers = executeOpcode(mappings[instruction[0]]!!.first(), instruction.drop(1), registers)
        }
        registers[0]
    }
}
