package Day22_2

import Day22_2.Tool.*
import Day22_2.Type.*
import solveInputs
import kotlin.math.abs

enum class Type {
    ROCKY, NARROW, WET;

    fun getLegalTools(): List<Tool> {
        return when (this) {
            ROCKY -> listOf(TORCH, GEAR)
            WET -> listOf(GEAR, NEITHER)
            NARROW -> listOf(TORCH, NEITHER)
        }
    }

}

enum class Tool {
    TORCH, GEAR, NEITHER
}

class Cave(private val target: Pos, private val depth: Int) {

    private val geologicalIndices = mutableMapOf<Pos, Int>()
    private val erosionLevels = mutableMapOf<Pos, Int>()
    private val types = mutableMapOf<Pos, Type>()

    operator fun get(position: Pos): Type {
        return getType(position)
    }

    private fun getErosionLevel(position: Pos): Int {
        return erosionLevels[position] ?: run {
            return ((getGeologicIndex(position) + depth) % 20183).also { erosionLevels[position] = it }
        }
    }

    private fun getGeologicIndex(position: Pos): Int {
        return geologicalIndices[position] ?: run {
            return when {
                position == target -> return 0
                position.y == 0 -> return position.x * 16807
                position.x == 0 -> return position.y * 48271
                else -> getErosionLevel(Pos(position.x - 1, position.y)) * getErosionLevel(Pos(position.x, position.y - 1))
            }.also { geologicalIndices[position] = it }
        }
    }

    private fun getType(position: Pos): Type {
        return types[position] ?: run {
            return when (getErosionLevel(position) % 3) {
                0 -> ROCKY
                1 -> WET
                2 -> NARROW
                else -> throw Exception()
            }.also { types[position] = it }
        }
    }

}

data class Pos(val x: Int, val y: Int)

fun getNeighbors(position: Pos): List<Pos> {
    return listOf(
        Pos(position.x - 1, position.y),
        Pos(position.x + 1, position.y),
        Pos(position.x, position.y - 1),
        Pos(position.x, position.y + 1)
    ).filter { it.x >= 0 && it.y >= 0 }
}

fun Cave.getPossibleActions(state: State): List<Action> {
    val validMovements = getNeighbors(state.position)
        .filter { state.tool in this[it].getLegalTools() }
        .map { Movement(state.position, it) }
    val otherTool = this[state.position].getLegalTools().first { it != state.tool }
    return validMovements + ToolSwitch(state.tool, otherTool)
}

fun manhattanDistance(a: Pos, b: Pos): Int {
    return abs(a.x - b.x) + abs(a.y - b.y)
}

sealed class Action {

    fun getCost(): Int {
        return when (this) {
            is Movement -> 1
            is ToolSwitch -> 7
        }
    }

}
data class Movement(val from: Pos, val to: Pos) : Action()
data class ToolSwitch(val from: Tool, val to: Tool) : Action()
data class State(val position: Pos, val tool: Tool) {

    fun perform(action: Action): State {
        return when (action) {
            is Movement -> State(action.to, tool)
            is ToolSwitch -> State(position, action.to)
        }
    }

    fun undo(action: Action): State {
        return when (action) {
            is Movement -> State(action.from, tool)
            is ToolSwitch -> State(position, action.from)
        }
    }

}
data class Cost(val cost: Int, val action: Action?)

private fun findPath(cave: Cave, from: State, to: State): List<Action>? {
    val visited = mutableSetOf<State>()
    val unvisited = mutableSetOf<State>()
    val costs = mutableMapOf<State, Cost>()
    costs[from] = Cost(0, null)
    var current = from
    while (true) {
        cave.getPossibleActions(current).forEach { action ->
            val newState = current.perform(action)
            if (newState !in visited) {
                unvisited += newState
            }
            val cost = costs[current]!!.cost + action.getCost()
            if (cost < (costs[newState]?.cost ?: Int.MAX_VALUE)) {
                costs[newState] = Cost(cost, action)
            }
        }
        unvisited -= current
        visited += current
        if (current == to) {
            val actions = mutableListOf<Action>()
            while (costs[current]!!.action != null) {
                actions += costs[current]!!.action!!
                current = current.undo(costs[current]!!.action!!)
            }
            return actions.reversed()
        }
        current = unvisited.minBy { costs[it]!!.cost + manhattanDistance(it.position, to.position) } ?: break
    }
    return null
}

fun main() {
    solveInputs { input ->
        val depth = input.lines().first().substringAfter(" ").toInt()
        val targetCoordinates = input.lines()[1].substringAfter(" ").split(",").map(String::toInt)
        val target = Pos(targetCoordinates[0], targetCoordinates[1])
        val cave = Cave(target, depth)
        val path = findPath(cave, State(Pos(0, 0), TORCH), State(target, TORCH))!!
        path.forEach {
            println(it)
        }
        val time = path.map { it.getCost() }.sum()
        time
    }
}
