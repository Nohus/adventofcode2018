package Day21_2

fun main() {
    println(program())
}

fun program(): Int {
    val numbers = mutableListOf<Int>()
    var c = 65536
    var d = 14070682
    while (true) {
        d += (c and 255)
        d = d and 16777215
        d *= 65899
        d = d and 16777215
        if (256 > c) {
            if (numbers.contains(d)) {
                return numbers.last()
            }
            numbers += d
            c = d or 65536
            d = 14070682
        } else {
            c /= 256
        }
    }
}
