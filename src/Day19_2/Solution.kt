package Day19_2

fun main() {
    val number = 10_551_275
    val sum = (1..number).filter { number % it == 0 }.sum()
    println("Answer: $sum")
}
