package Day5_1

import solveInputs
import kotlin.math.max

fun Char.swapCase() = if (isLowerCase()) toUpperCase() else toLowerCase()

fun react(input: String): String {
    var polymer = input
    var i = 0
    while (i < polymer.lastIndex) {
        if (polymer[i].swapCase() == polymer[i + 1]) {
            polymer = polymer.removeRange(i, i + 2)
            i = max(i - 1, 0)
        } else {
            i++
        }
    }
    return polymer
}

fun main() {
    solveInputs { input ->
        "${react(input).length}"
    }
}
