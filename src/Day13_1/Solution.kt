package Day13_1

import Day13_1.Direction.*
import Day13_1.TrackType.*
import solveInputs

enum class Direction {
    UP, RIGHT, DOWN, LEFT
}

enum class TrackType {
    HORIZONTAL, VERTICAL, SLASH, BACKSLASH, JUNCTION, NONE
}

enum class Turn {
    LEFT, STRAIGHT, RIGHT;

    fun next(): Turn {
        return when (this) {
            LEFT -> STRAIGHT
            STRAIGHT -> RIGHT
            RIGHT -> LEFT
        }
    }

}

data class Cart(var direction: Direction, var nextTurn: Turn = Turn.LEFT)

data class Track(val type: TrackType, var cart: Cart? = null) {
    override fun toString(): String {
        if (cart != null) {
            return when (cart!!.direction) {
                UP -> "^"
                RIGHT -> ">"
                DOWN -> "v"
                LEFT -> "<"
            }
        } else {
            return when (type) {
                HORIZONTAL -> "-"
                VERTICAL -> "|"
                SLASH -> "/"
                BACKSLASH -> "\\"
                JUNCTION -> "+"
                NONE -> " "
            }
        }
    }
}

fun printMap(map: Array<Array<Track>>) {
    map.forEachIndexed { y, column ->
        println(column.joinToString(""))
    }
}

fun main() {
    solveInputs { input ->
        val height = 160
        val width = 160
        var map = Array(height) { Array(width) { Track(NONE) } }
        input.lines().forEachIndexed { y, line ->
            line.chunked(1).forEachIndexed { x, char ->
                val track = when (char) {
                    "/" -> Track(SLASH)
                    "\\" -> Track(BACKSLASH)
                    "-" -> Track(HORIZONTAL)
                    "|" -> Track(VERTICAL)
                    "<" -> Track(HORIZONTAL, Cart(LEFT))
                    ">" -> Track(HORIZONTAL, Cart(RIGHT))
                    "^" -> Track(VERTICAL, Cart(UP))
                    "v" -> Track(VERTICAL, Cart(DOWN))
                    "+" -> Track(JUNCTION)
                    " " -> Track(NONE)
                    else -> throw Exception(char)
                }
                //println("$x $y")
                map[y][x] = track
            }
        }
        //printMap(map)
        var time = 0
        var crash = ""
        while (true) {
            time++
            val newMap = Array(height) { Array(width) { Track(NONE) } }
            for (y in 0 until height) {
                for (x in 0 until width) {
                    newMap[y][x] = Track(map[y][x].type)
                }
            }
            for (y in 0 until height) {
                for (x in 0 until width) {
                    map[y][x].cart?.let { cart ->
                        val direction = cart.direction
                        val target = when (direction) {
                            UP -> Pair(x, y - 1)
                            DOWN -> Pair(x, y + 1)
                            LEFT -> Pair(x - 1, y)
                            RIGHT -> Pair(x + 1, y)
                        }
                        val newDirection: Direction
                        val track = map[target.second][target.first]
                        newDirection = when {
                            track.type == SLASH && direction == UP -> RIGHT
                            track.type == SLASH && direction == DOWN -> LEFT
                            track.type == SLASH && direction == RIGHT -> UP
                            track.type == SLASH && direction == LEFT -> DOWN
                            track.type == BACKSLASH && direction == UP -> LEFT
                            track.type == BACKSLASH && direction == DOWN -> RIGHT
                            track.type == BACKSLASH && direction == RIGHT -> DOWN
                            track.type == BACKSLASH && direction == LEFT -> UP
                            track.type == JUNCTION -> {
                                when (direction) {
                                    UP -> {
                                        when (cart.nextTurn) {
                                            Turn.LEFT -> LEFT
                                            Turn.STRAIGHT -> UP
                                            Turn.RIGHT -> RIGHT
                                        }
                                    }
                                    RIGHT -> {
                                        when (cart.nextTurn) {
                                            Turn.LEFT -> UP
                                            Turn.STRAIGHT -> RIGHT
                                            Turn.RIGHT -> DOWN
                                        }
                                    }
                                    DOWN -> {
                                        when (cart.nextTurn) {
                                            Turn.LEFT -> RIGHT
                                            Turn.STRAIGHT -> DOWN
                                            Turn.RIGHT -> LEFT
                                        }
                                    }
                                    LEFT -> {
                                        when (cart.nextTurn) {
                                            Turn.LEFT -> DOWN
                                            Turn.STRAIGHT -> LEFT
                                            Turn.RIGHT -> UP
                                        }
                                    }
                                }
                            }
                            else -> direction
                        }
                        if (track.type == JUNCTION) {
                            cart.nextTurn = cart.nextTurn.next()
                        }
                        cart.direction = newDirection
                        if (newMap[target.second][target.first].cart != null) {
                            println("Crash at ${target.first}x${target.second} at time $time")
                            if (crash.isEmpty()) {
                                crash = "${target.first},${target.second}"
                            }
                        } else {
                            newMap[target.second][target.first].cart = cart
                        }
                    }
                }
            }
            if (crash.isNotEmpty()) {
                break
            }
            map = newMap
            //printMap(map)
        }
        "$crash"
    }
}
