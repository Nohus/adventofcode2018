package Day7_1

import solveInputs

fun main() {
    solveInputs { input ->
        val requirements = input.lines()
            .map { it.substringAfter("Step ").substringBefore(" must") to it.substringAfter("step ").substringBefore(" can") }
        val todo = requirements.map { listOf(it.first, it.second) }.flatten().toMutableSet()
        val sequence = mutableListOf<String>()
        while (todo.isNotEmpty()) {
            todo.filter { candidate -> requirements.filter { it.second == candidate }.map { it.first }.all { it in sequence } }.sorted().first().let {
                sequence += it
                todo -= it
            }
        }
        sequence.joinToString("")
    }
}
