package Day3_2

import solveInputs

data class Rect(val id: Int, val x: Int, val y: Int, val width: Int, val height: Int) {
    fun overlaps(other: Rect): Boolean {
        return x + width >= other.x && x <= other.x + other.width && y + height >= other.y && y <= other.y + other.height
    }
}

fun main() {
    solveInputs { input ->
        val claims = input.lines().map { it.drop(1).filterNot { it == ' ' }.split(Regex("\\D"))
            .map(String::toInt).let { n -> Rect(n[0], n[1], n[2], n[3], n[4]) } }
        outer@for (a in claims) {
            for (b in claims) {
                if (a.id != b.id && a.overlaps(b)) {
                    continue@outer
                }
            }
            return@solveInputs "${a.id}"
        }
        ""
    }
}
