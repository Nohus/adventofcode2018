package Day18_1

import Day18_1.Acre.*
import solveInputs

data class Pos(val x: Int, val y: Int)

enum class Acre {
    GROUND, TREES, LUMBERYARD
}

class Area {

    private val array = Array(width) { Array(height) { GROUND } }

    operator fun get(position: Pos): Acre {
        return array[position.x][position.y]
    }

    operator fun set(position: Pos, value: Acre) {
        array[position.x][position.y] = value
    }

}

const val width = 50
const val height = 50
var area = Area()

fun printArea() {
    for (y in 0 until height) {
        for (x in 0 until width) {
            val acre = area[Pos(x, y)]
            val character = when (acre) {
                GROUND -> "."
                TREES -> "|"
                LUMBERYARD -> "#"
            }
            print(character)
        }
        println("")
    }
}

fun Pos.isInBounds(): Boolean {
    return x in 0..(width - 1) && y in 0..(height - 1)
}

fun Pos.getNeighbours(): List<Acre> {
    return listOf(
        Pos(x - 1, y - 1), Pos(x, y - 1), Pos(x + 1, y - 1),
        Pos(x - 1, y), Pos(x + 1, y),
        Pos(x - 1, y + 1), Pos(x, y + 1), Pos(x + 1, y + 1)
    ).filter { it.isInBounds() }.map { area[it] }
}

fun Pos.getNextGeneration(): Acre {
    val acre = area[this]
    return when (acre) {
        GROUND -> if (getNeighbours().count { it == TREES } >= 3) TREES else acre
        TREES -> if (getNeighbours().count { it == LUMBERYARD } >= 3) LUMBERYARD else acre
        LUMBERYARD -> if (getNeighbours().let { it.any { it == LUMBERYARD } && it.any { it == TREES } }) LUMBERYARD else GROUND
    }
}

fun getAllPositions(): List<Pos> {
    val positions = mutableListOf<Pos>()
    for (x in 0 until width) {
        for (y in 0 until height) {
            positions += Pos(x, y)
        }
    }
    return positions
}

fun tickArea() {
    val newArea = Area()
    getAllPositions().forEach { pos -> newArea[pos] = pos.getNextGeneration() }
    area = newArea
}

fun main() {
    solveInputs { input ->
        input.lines().forEachIndexed { y, line ->
            line.forEachIndexed { x, character ->
                area[Pos(x, y)] = when (character) {
                    '.' -> GROUND
                    '|' -> TREES
                    '#' -> LUMBERYARD
                    else -> throw Exception("Unknown input character: $character")
                }
            }
        }
        val targetMinute = 10
        for (minute in 1..targetMinute) {
            tickArea()
            println("\nAfter $minute minutes:")
            printArea()
        }
        val wood = getAllPositions().map { if (area[it] == TREES) 1 else 0 }.sum()
        val lumberyards = getAllPositions().map { if (area[it] == LUMBERYARD) 1 else 0 }.sum()
        wood * lumberyards
    }
}
