package Day24_1

import Day24_1.Army.IMMUNE_SYSTEM
import Day24_1.Army.INFECTION
import solveInputs
import kotlin.math.max
import kotlin.math.min

enum class AttackType {
    FIRE, BLUDGEONING, COLD, RADIATION, SLASHING;

    companion object {

        fun parse(text: String): AttackType {
            return when (text) {
                "fire" -> FIRE
                "bludgeoning" -> BLUDGEONING
                "cold" -> COLD
                "radiation" -> RADIATION
                "slashing" -> SLASHING
                else -> throw Exception("Invalid attack type: $text")
            }
        }

    }

}

enum class Army {
    IMMUNE_SYSTEM, INFECTION
}

data class Group(
    var units: Int,
    val health: Int,
    val weaknesses: Set<AttackType>,
    val immunities: Set<AttackType>,
    val attackType: AttackType,
    val damage: Int,
    val initiative: Int,
    val army: Army,
    val id: Int
) {

    val effectivePower
        get() = units * damage

    fun getPotentialDamage(attacker: Group): Int {
        return when {
            attacker.attackType in weaknesses -> attacker.effectivePower * 2
            attacker.attackType in immunities -> 0
            else -> attacker.effectivePower
        }
    }

    fun applyDamageFrom(attacker: Group) {
        val potentialDamage = getPotentialDamage(attacker)
        val unitsLost = min(potentialDamage / health, units)
        units -= unitsLost
    }

    override fun toString(): String {
        return when (army) {
            IMMUNE_SYSTEM -> "Immune System group $id"
            INFECTION -> "Infection group $id"
        }
    }

}

fun parseArmy(text: String, army: Army): List<Group> {
    var id = 0
    return text.trim().lines().map {
        id++
        val units = it.substringBefore(" units").toInt()
        val health = it.substringAfter("with ").substringBefore(" hit points").toInt()
        val weaknesses = mutableSetOf<AttackType>()
        val immunities = mutableSetOf<AttackType>()
        if (it.contains("(")) {
            val specials = it.substringAfter("(").substringBefore(")")
            specials.split("; ").forEach { special ->
                val types = special.substringAfter("to ").split(", ").map { AttackType.parse(it) }
                if (special.startsWith("weak")) {
                    weaknesses += types
                } else if (special.startsWith("immune")) {
                    immunities += types
                }
            }
        }
        val attackType = AttackType.parse(it.substringBefore(" damage").substringAfterLast(" "))
        val damage = it.substringAfter("does ").substringBefore(" ").toInt()
        val initiative = it.substringAfter("initiative ").toInt()
        Group(units, health, weaknesses, immunities, attackType, damage, initiative, army, id)
    }
}

fun areFighting(groups: List<Group>): Boolean {
    return groups.count { it.army == IMMUNE_SYSTEM } > 0 && groups.count { it.army == INFECTION } > 0
}

fun printStats(groups: List<Group>, army: Army) {
    if (army == IMMUNE_SYSTEM) {
        println("Immune System:")
    } else if (army == INFECTION) {
        println("Infection:")
    }
    val armyGroups = groups.filter { it.army == army }
    if (armyGroups.isNotEmpty()) {
        armyGroups.forEach {
            println("Group ${it.id} contains ${it.units} units")
        }
    } else {
        println("No groups remain.")
    }
}

fun main() {
    solveInputs { input ->
        val immuneSystemString = input.substringBefore("Infection:").substringAfter("Immune System:")
        val infectionString = input.substringAfter("Infection:")
        var groups = parseArmy(immuneSystemString, IMMUNE_SYSTEM) + parseArmy(infectionString, INFECTION)
        while (areFighting(groups)) {
            // Target selection
            val attackMapping = mutableMapOf<Group, Group>()
            groups.sortedWith(compareBy({ it.effectivePower }, { it.initiative })).reversed().map { attacker ->
                val targets = groups.filter { it.army != attacker.army }.filter { it !in attackMapping.values }
                targets.sortedWith(compareBy({ it.getPotentialDamage(attacker) }, { it.effectivePower }, { it.initiative })).reversed().firstOrNull()?.let {
                    if (it.getPotentialDamage(attacker) > 0) {
                        attackMapping[attacker] = it
                    }
                }
            }
            // Attacking
            attackMapping.entries.sortedByDescending { it.key.initiative }.forEach {
                val attacker = it.key
                val defender = it.value
                if (attacker.units > 0) {
                    defender.applyDamageFrom(attacker)
                }
            }
            groups = groups.filter { it.units > 0 }
        }
        //printStats(groups, IMMUNE_SYSTEM)
        //printStats(groups, INFECTION)
        val answer = max(groups.filter { it.army == IMMUNE_SYSTEM }.sumBy { it.units }, groups.filter { it.army == INFECTION }.sumBy { it.units })
        answer
    }
}
