package Day4_2

import solveInputs
import java.util.*

data class Time(val month: Int, val day: Int, val hour: Int, val minute: Int, val info: String)

fun main() {
    solveInputs { input ->
        val times = input.lines().map {
            val numbers = it.split(Regex("\\D")).map { it.toIntOrNull() ?: 0 }
            val info = it.substringAfter("[ ")
            Time(numbers[2], numbers[3], numbers[4], numbers[5], info)
        }.sortedWith(compareBy({ it.month }, { it.day }, { it.hour }, { it.minute }))
        val guards = mutableSetOf<Int>()
        val guardMinutes = mutableMapOf<Int, MutableMap<Int, Int>>()
        var currentGuard = 0
        var sleepStart: Time? = null
        times.forEach {
            when {
                it.info.contains("begins shift") -> {
                    val id = it.info.substringAfter("#").substringBefore(" ")
                    guards += id.toInt()
                    currentGuard = id.toInt()
                }
                it.info.contains("falls") -> sleepStart = it
                it.info.contains("wakes") -> {
                    sleepStart?.let {sleepStart ->
                        val start = GregorianCalendar(1518, sleepStart.month - 1, sleepStart.day, sleepStart.hour, sleepStart.minute).timeInMillis
                        val end = GregorianCalendar(1518, it.month - 1, it.day, it.hour, it.minute).timeInMillis
                        val minutes = ((end - start) / 1000) / 60
                        val minuteStart = sleepStart.minute
                        for (minute in minuteStart until (minuteStart + minutes)) {
                            val minuteSafe = (minute % 60).toInt()
                            val minutesMap = guardMinutes.getOrDefault(currentGuard, mutableMapOf())
                            minutesMap[minuteSafe] = minutesMap.getOrDefault(minuteSafe, 0) + 1
                            guardMinutes[currentGuard] = minutesMap
                        }
                    }
                    sleepStart = null
                }
            }
        }
        val guard = guards.maxBy { guardMinutes[it]?.entries?.sortedByDescending { it.value }?.first()?.value ?: 0 }!!
        val minute = guardMinutes[guard]?.entries?.sortedByDescending { it.value }?.first()?.key!!
        "${guard * minute}"
    }
}
