package Day3_1

import solveInputs

data class Rect(val id: Int, val x: Int, val y: Int, val width: Int, val height: Int)

fun main() {
    solveInputs { input ->
        val fabric = mutableMapOf<Pair<Int, Int>, Int>().withDefault { 0 }
        input.lines().map { it.drop(1).filterNot { it == ' ' }.split(Regex("\\D"))
            .map(String::toInt).let { n -> Rect(n[0], n[1], n[2], n[3], n[4]) } }.forEach {
                for (x in it.x until (it.x + it.width)) {
                    for (y in it.y until (it.y + it.height)) {
                        fabric[x to y] = fabric.getValue(x to y) + 1
                    }
                }
        }
        val count = fabric.values.filter { it >= 2 }.size
        "$count"
    }
}
