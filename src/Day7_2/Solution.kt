package Day7_2

import solveInputs

fun main() {
    solveInputs { input ->
        val requirements = input.lines()
            .map { it.substringAfter("Step ").substringBefore(" must") to it.substringAfter("step ").substringBefore(" can") }
        val todo = requirements.map { listOf(it.first, it.second) }.flatten().toMutableSet()
        val sequence = mutableListOf<String>()
        var workers = MutableList(5) { 0 }
        var workedOn = mutableMapOf<String, Int>()
        var second = 0
        while (todo.isNotEmpty() || workedOn.isNotEmpty()) {
            while (workers.any { it == 0 }) {
                val choice = todo.filter { candidate -> requirements.filter { it.second == candidate }.map { it.first }.all { it in sequence } }.sorted().firstOrNull() ?: break
                val time = 61 + choice[0].toInt() - 'A'.toInt()
                todo -= choice
                workers.remove(0)
                workers.add(time)
                workedOn[choice] = time
            }
            second++
            workers = workers.map { if (it == 0) 0 else it - 1 }.toMutableList()
            workedOn = workedOn.mapValues { it.value - 1 }.toMutableMap()
            workedOn.filter { it.value == 0 }.forEach { done ->
                workedOn.remove(done.key)
                sequence.add(done.key)
            }
        }
        "$second"
    }
}
