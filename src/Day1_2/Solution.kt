package Day1_2

import solveInputs

fun main() {
    solveInputs { input ->
        val seen = mutableSetOf<Int>()
        var sum = 0
        while (true) {
            for (number in input.lines().map { it.toInt() }) {
                seen += sum
                sum += number
                if (sum in seen) {
                    return@solveInputs "$sum"
                }
            }
        }
        ""
    }
}
