package Day2_2

import solveInputs

fun main() {
    solveInputs { input ->
        val boxes = input.lines()
        boxes.forEach { box ->
            boxes.forEach { otherBox ->
                val letters = box.chunked(1)
                val otherLetters = otherBox.chunked(1)
                val differences = mutableListOf<String>()
                for (i in 0..letters.lastIndex) {
                    if (letters[i] != otherLetters[i]) {
                        differences.add(letters[i])
                    }
                }
                if (differences.size == 1) {
                    return@solveInputs box.replace(differences.first(), "")
                }
            }
        }
        ""
    }
}
