package Day10_1

import solveInputs
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import java.util.regex.Pattern
import javax.imageio.ImageIO

data class Vector(val x: Int, val y: Int)

data class Point(val position: Vector, val velocity: Vector)

fun main() {
    solveInputs { input ->
        var points = input.lines().map {
            val split = it.split(Pattern.compile("[^\\d-]+")).subList(1, 5).map(String::toInt)
            Point(Vector(split[0], split[1]), Vector(split[2], split[3]))
        }
        var iteration = 0
        var lastDimensions = Vector(Integer.MAX_VALUE, Integer.MAX_VALUE)
        while (true) {
            val newPoints = points.map { Point(Vector(it.position.x + it.velocity.x, it.position.y + it.velocity.y), it.velocity) }
            var minX = newPoints.minBy { it.position.x }!!.position.x
            val maxX = newPoints.maxBy { it.position.x }!!.position.x
            var minY = newPoints.minBy { it.position.y }!!.position.y
            val maxY = newPoints.maxBy { it.position.y }!!.position.y
            val dimensions = Vector(maxX - minX, maxY - minY)
            if (dimensions.x <= lastDimensions.x) {
                lastDimensions = dimensions
                points = newPoints
                iteration++
                continue
            }

            val display = mutableMapOf<Vector, Boolean>()
            points.forEach {
                display[it.position] = true
            }
            val image = BufferedImage(lastDimensions.x + 1, lastDimensions.y + 1, BufferedImage.TYPE_BYTE_BINARY)
            minX = points.minBy { it.position.x }!!.position.x
            minY = points.minBy { it.position.y }!!.position.y
            for (y in minY..maxY) {
                for (x in minX..maxX) {
                    if (display[Vector(x, y)] == true) {
                        image.setRGB(x - minX, y - minY, Color.WHITE.rgb)
                    }
                }
            }
            ImageIO.write(image, "png", File("$iteration.png"))
            return@solveInputs "$iteration.png"
        }
        ""
    }
}
