package Day4_1

import solveInputs
import java.util.*

data class Time(val month: Int, val day: Int, val hour: Int, val minute: Int, val info: String)

fun main() {
    solveInputs { input ->
        val times = input.lines().map {
            val numbers = it.split(Regex("\\D")).map { it.toIntOrNull() ?: 0 }
            val info = it.substringAfter("[ ")
            Time(numbers[2], numbers[3], numbers[4], numbers[5], info)
        }.sortedWith(compareBy({ it.month }, { it.day }, { it.hour }, { it.minute }))
        val guardMostMinutes = mutableMapOf<Int, MutableMap<Int, Int>>()
        times.forEachIndexed { index, time ->
            if (time.info.contains("begins shift")) {
                val id = time.info.substringAfter("#").substringBefore(" ").toInt()
                val guardTimes = times.subList(index + 1, times.size).takeWhile { !it.info.contains("begins shift") }
                var sleepStart: Time? = null
                guardTimes.forEach {
                    when {
                        it.info.contains("falls") -> sleepStart = it
                        it.info.contains("wakes") -> {
                            sleepStart?.let {sleepStart ->
                                val start = GregorianCalendar(1518, sleepStart.month - 1, sleepStart.day, sleepStart.hour, sleepStart.minute).timeInMillis
                                val end = GregorianCalendar(1518, it.month - 1, it.day, it.hour, it.minute).timeInMillis
                                val minutes = ((end - start) / 1000) / 60
                                val minuteStart = sleepStart.minute
                                for (m in minuteStart until (minuteStart + minutes)) {
                                    val minuteSafe = (m % 60).toInt()
                                    val minutesMap = guardMostMinutes.getOrDefault(id, mutableMapOf())
                                    minutesMap[minuteSafe] = minutesMap.getOrDefault(minuteSafe, 0) + 1
                                    guardMostMinutes[id] = minutesMap
                                }
                            }
                            sleepStart = null
                        }
                    }
                }
            }
        }
        val guard = guardMostMinutes.entries.map { it.key to it.value.values.sum() }.sortedByDescending { it.second }.first().first
        val mostCommon = guardMostMinutes[guard]!!.entries.sortedByDescending { it.value }.first().key
        "${guard * mostCommon}"
    }
}
