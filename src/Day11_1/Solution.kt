package Day11_1

import solveInputs

fun powerLevel(x: Int, y: Int, serial: Int): Int {
    val rackId = x + 10
    return ((((rackId * y) + serial) * rackId).toString().reversed().getOrNull(2)?.toString()?.toInt() ?: 0) - 5
}

fun main() {
    solveInputs { input ->
        val serial = input.toInt()
        var max = 0
        var bestX = 0
        var bestY = 0
        for (x in 1..(300 - 2)) {
            for (y in 1..(300 - 2)) {
                var sum = 0
                for (squareX in x..(x + 2)) {
                    for (squareY in y..(y + 2)) {
                        sum += powerLevel(squareX, squareY, serial)
                    }
                }
                if (sum > max) {
                    max = sum
                    bestX = x
                    bestY = y
                }
            }
        }
        "$bestX,$bestY"
    }
}
