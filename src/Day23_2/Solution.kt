package Day23_2

import solveInputs
import kotlin.math.abs

data class Pos(val x: Int, val y: Int, val z: Int) {

    fun distanceTo(other: Pos): Int {
        return abs(x - other.x) + abs(y - other.y) + abs(z - other.z)
    }

}

data class Sphere(val center: Pos, val radius: Int) {

    fun split(distance: Int): List<Sphere> {
        return mutableListOf<Sphere>().also {
            for (x in -1..1) {
                for (y in -1..1) {
                    for (z in -1..1) {
                        it += Sphere(
                            Pos(
                                center.x + x * distance,
                                center.y + y * distance,
                                center.z + z * distance
                            ), distance
                        )
                    }
                }
            }
        }
    }

    fun isInRange(other: Sphere): Boolean {
        return center.distanceTo(other.center) <= radius + other.radius
    }

}

fun main() {
    solveInputs { input ->
        val bots = input.lines().map { line ->
            val position = line.substringAfter("<").substringBefore(">").split(",").map(String::toInt).let { Pos(it[0], it[1], it[2]) }
            val radius = line.substringAfter("r=").toInt()
            Sphere(position, radius)
        }
        var radius = bots.map { maxOf(it.center.x, it.center.y, it.center.z) }.max()!!
        var spheres = setOf(Sphere(Pos(0, 0, 0), radius))
        while (radius > 0) {
            radius /= 2
            val splitSpheres = spheres.flatMap { sphere -> sphere.split(radius).map { it to bots.count { bot -> it.isInRange(bot) } } }
            val maxBots = splitSpheres.maxBy { it.second }!!.second
            spheres = splitSpheres.filter { it.second == maxBots }.map { it.first }.toSet()
        }
        spheres.first().center.distanceTo(Pos(0, 0, 0))
    }
}
