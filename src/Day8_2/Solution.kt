package Day8_2

import solveInputs

fun readNode(list: MutableList<Int>): Int {
    val nodes = list.removeAt(0)
    val entries = list.removeAt(0)
    var sum = 0
    if (nodes == 0) {
        repeat(entries) {
            sum += list.removeAt(0)
        }
    } else {
        val nodeValues = mutableListOf<Int>()
        repeat(nodes) {
            nodeValues.add(readNode(list))
        }
        val indices = mutableListOf<Int>()
        repeat(entries) {
            indices.add(list.removeAt(0))
        }
        for (i in indices) {
            val index = i - 1
            if (nodeValues.getOrNull(index) != null) {
                sum += nodeValues[index]
            }
        }
    }
    return sum
}

fun main() {
    solveInputs { input ->
        val list = input.split(" ").map { it.toInt() }.toMutableList()
        "${readNode(list)}"
    }
}
