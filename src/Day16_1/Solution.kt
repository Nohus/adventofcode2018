package Day16_1

import solveInputs

fun parseRegisters(text: String): List<Int> {
    return text
        .substringAfter("[")
        .substringBefore("]")
        .split(", ")
        .map(String::toInt)
}

fun executeOpcode(opcode: String, arguments: List<Int>, inputRegisters: List<Int>): List<Int> {
    val registers = inputRegisters.toMutableList()
    val a = arguments[0]
    val b = arguments[1]
    val c = arguments[2]
    registers[c] = when (opcode) {
        "addr" -> registers[a] + registers[b]
        "addi" -> registers[a] + b
        "mulr" -> registers[a] * registers[b]
        "muli" -> registers[a] * b
        "banr" -> registers[a] and registers[b]
        "bani" -> registers[a] and b
        "borr" -> registers[a] or registers[b]
        "bori" -> registers[a] or b
        "setr" -> registers[a]
        "seti" -> a
        "gtir" -> if (a > registers[b]) 1 else 0
        "gtri" -> if (registers[a] > b) 1 else 0
        "gtrr" -> if (registers[a] > registers[b]) 1 else 0
        "eqir" -> if (a == registers[b]) 1 else 0
        "eqri" -> if (registers[a] == b) 1 else 0
        "eqrr" -> if (registers[a] == registers[b]) 1 else 0
        else -> throw Exception("Invalid opcode")
    }
    return registers
}

fun main() {
    solveInputs { input ->
        val opcodes = listOf("addr", "addi", "mulr", "muli", "banr", "bani", "borr", "bori", "setr", "seti", "gtir", "gtri", "gtrr", "eqir", "eqri", "eqrr")
        var counter = 0
        input.lines().chunked(3).forEach {
            val beforeRegisters = parseRegisters(it[0])
            val afterRegisters = parseRegisters(it[2])
            val instruction = it[1].split(" ").map(String::toInt)
            val possibleOpcodes = opcodes.filter { executeOpcode(it, instruction.drop(1), beforeRegisters) == afterRegisters }
            counter += if (possibleOpcodes.size >= 3) 1 else 0
        }
        counter
    }
}
