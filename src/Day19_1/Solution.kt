package Day19_1

import solveInputs

fun executeOpcode(opcode: String, arguments: List<Int>, inputRegisters: List<Int>): MutableList<Int> {
    val registers = inputRegisters.toMutableList()
    val a = arguments[0]
    val b = arguments[1]
    val c = arguments[2]
    registers[c] = when (opcode) {
        "addr" -> registers[a] + registers[b]
        "addi" -> registers[a] + b
        "mulr" -> registers[a] * registers[b]
        "muli" -> registers[a] * b
        "banr" -> registers[a] and registers[b]
        "bani" -> registers[a] and b
        "borr" -> registers[a] or registers[b]
        "bori" -> registers[a] or b
        "setr" -> registers[a]
        "seti" -> a
        "gtir" -> if (a > registers[b]) 1 else 0
        "gtri" -> if (registers[a] > b) 1 else 0
        "gtrr" -> if (registers[a] > registers[b]) 1 else 0
        "eqir" -> if (a == registers[b]) 1 else 0
        "eqri" -> if (registers[a] == b) 1 else 0
        "eqrr" -> if (registers[a] == registers[b]) 1 else 0
        else -> throw Exception("Invalid opcode")
    }
    return registers
}

data class Instruction(val opcode: String, val arguments: List<Int>)

fun main() {
    solveInputs { input ->
        val pcRegister = input.lines().first().substringAfterLast(" ").toInt()
        val instructions = input.lines().drop(1).map {
            val opcode = it.substringBefore(" ")
            val arguments = it.substringAfter(" ").split(" ").map(String::toInt)
            Instruction(opcode, arguments)
        }
        var pc = 0
        var registers = (0..5).map { 0 }.toMutableList()
        while (true) {
            val instruction = instructions.getOrNull(pc) ?: break
            registers[pcRegister] = pc
            registers = executeOpcode(instruction.opcode, instruction.arguments, registers)
            pc = registers[pcRegister]
            pc++
        }
        registers[0]
    }
}
