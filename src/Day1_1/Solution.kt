package Day1_1

import solveInputs

fun main() {
    solveInputs { input ->
        "${input.lines().map { it.toInt() }.sum()}"
    }
}
