package Day9_1

import solveInputs
import java.util.*
import kotlin.math.abs

fun<T> ArrayDeque<T>.rotate(count: Int) {
    if (count > 0) {
        for (i in 0 until count) {
            addFirst(removeLast())
        }
    } else {
        for (i in 0 until abs(count)) {
            addLast(removeFirst())
        }
    }
}

fun main() {
    solveInputs { input ->
        val elves = input.substringBefore(" ").toInt()
        val marbles = input.substringAfter("worth ").substringBefore(" ").toLong()
        val scores = Array<Long>(elves) { 0 }
        val list = ArrayDeque<Long>()
        list += 0
        var marble = 0L
        while (marble < marbles) {
            marble++
            if (marble % 23L == 0L) {
                list.rotate(-6)
                scores[(marble % elves).toInt()] += list.removeFirst() + marble
            } else {
                list.rotate(2)
                list.addLast(marble)
            }
        }
        "${scores.max()}"
    }
}

