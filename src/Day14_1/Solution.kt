package Day14_1

import solveInputs

fun main() {
    solveInputs { input ->
        val limit = input.toInt()
        val recipes = mutableListOf(3, 7)
        var elf1 = 0
        var elf2 = 1
        while(recipes.size < (limit + 10)) {
            recipes += (recipes[elf1] + recipes[elf2]).toString().chunked(1).map { it.toInt() }
            elf1 = (elf1 + 1 + recipes[elf1]) % recipes.size
            elf2 = (elf2 + 1 + recipes[elf2]) % recipes.size
        }
        recipes.takeLast(10).joinToString("")
    }
}
