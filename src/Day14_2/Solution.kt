package Day14_2

import solveInputs

fun main() {
    solveInputs { input ->
        val recipes = mutableListOf(3, 7)
        var elf1 = 0
        var elf2 = 1
        while (!recipes.takeLast(input.length + 2).joinToString("").contains(input)) {
            recipes += (recipes[elf1] + recipes[elf2]).toString().chunked(1).map { it.toInt() }
            elf1 = (elf1 + 1 + recipes[elf1]) % recipes.size
            elf2 = (elf2 + 1 + recipes[elf2]) % recipes.size
        }
        (recipes.joinToString("").indexOf(input)).toString()
    }
}
