package Day20_1

import Day20_1.Direction.*
import Day20_1.Door.YES
import solveInputs
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

enum class Direction {

    NORTH, EAST, SOUTH, WEST;

    companion object {
        val validCharacters = listOf('N', 'E', 'S', 'W')
        fun fromCharacter(character: Char): Direction {
            return when (character) {
                'N' -> NORTH
                'E' -> EAST
                'S' -> SOUTH
                'W' -> WEST
                else -> throw Exception("Invalid direction character: $character")
            }
        }
    }

    fun reverse(): Direction {
        return when(this) {
            NORTH -> SOUTH
            EAST -> WEST
            SOUTH -> NORTH
            WEST -> EAST
        }
    }

}

data class Node(
    val directions: List<Direction>? = null,
    val route: Route? = null
)

data class Route(val branches: List<List<Node>>)

data class Pos(val x: Int, val y: Int) {

    fun move(direction: Direction): Pos {
        return when (direction) {
            NORTH -> Pos(x, y - 1)
            EAST -> Pos(x + 1, y)
            SOUTH -> Pos(x, y + 1)
            WEST -> Pos(x - 1, y)
        }
    }

}

enum class Door {
    YES, NO, UNKNOWN
}

data class Room(val doors: MutableMap<Direction, Door> = mutableMapOf())

fun getMatchingCloseParenthesis(text: String): Int {
    var current = 0
    var depth = 1
    while (depth > 0) {
        current++
        val next = text[current]
        if (next == '(') {
            depth++
        } else if (next == ')') {
            depth--
        }
    }
    return current
}

fun parseRoute(text: String): Route {
    if (text.contains('(')) {
        var unparsed = text
        val branches = mutableListOf<List<Node>>()
        val nodes = mutableListOf<Node>()
        val directions = mutableListOf<Direction>()
        while (unparsed.isNotEmpty()) {
            val next = unparsed[0]
            if (next in Direction.validCharacters) {
                directions += Direction.fromCharacter(next)
                unparsed = unparsed.drop(1)
            } else {
                if (directions.isNotEmpty()) {
                    nodes += Node(directions = directions.toMutableList())
                    directions.clear()
                }
                if (next == '(') {
                    val innerEnd = getMatchingCloseParenthesis(unparsed)
                    val innerRouteText = unparsed.substring(1, innerEnd)
                    unparsed = unparsed.substring(innerEnd + 1)
                    val innerRoute = parseRoute(innerRouteText)
                    nodes += Node(route = innerRoute)
                } else if (next == '|') {
                    unparsed = unparsed.drop(1)
                    branches += nodes.toMutableList()
                    nodes.clear()
                    if (unparsed.isEmpty()) {
                        branches += mutableListOf<Node>()
                    }
                }
            }
        }
        if (directions.isNotEmpty()) {
            nodes += Node(directions = directions.toMutableList())
            directions.clear()
        }
        if (nodes.isNotEmpty()) {
            branches += nodes
        }
        return Route(branches)
    } else {
        val nodes = text.split('|').map {
            val directions = it.toCharArray().map { Direction.fromCharacter(it) }
            Node(directions = directions)
        }
        val branches = nodes.map { listOf(it) }
        return Route(branches)
    }
}

val facility = mutableMapOf<Pos, Room>()

fun exploreRoute(startingPosition: Pos, route: Route) {
    for (branch in route.branches) {
        var position = startingPosition
        var room = facility[position]!!
        for (node in branch) {
            if (node.directions != null) {
                for (direction in node.directions) {
                    position = position.move(direction)
                    room.doors[direction] = YES
                    val newRoom = facility[position] ?: Room()
                    newRoom.doors[direction.reverse()] = YES
                    facility[position] = newRoom
                    room = newRoom
                }
                // drawFacility()
            } else if (node.route != null) {
                exploreRoute(position, node.route)
            }
        }
    }
}

fun getNeighbors(position: Pos): List<Pos> {
    val room = facility[position]!!
    val neighbors = mutableListOf<Pos>()
    if (room.doors[NORTH] == YES) {
        neighbors += Pos(position.x, position.y - 1)
    }
    if (room.doors[SOUTH] == YES) {
        neighbors += Pos(position.x, position.y + 1)
    }
    if (room.doors[EAST] == YES) {
        neighbors += Pos(position.x + 1, position.y)
    }
    if (room.doors[WEST] == YES) {
        neighbors += Pos(position.x - 1, position.y)
    }
    return neighbors
}

private fun bfs(from: Pos): List<List<Pos>> {
    val visited = mutableSetOf<Pos>()
    val unvisited = mutableListOf<Pos>()
    val paths = mutableMapOf<Pos, List<Pos>>()
    paths[from] = listOf(from)
    var current = from
    while (true) {
        unvisited += getNeighbors(current).filter { it !in unvisited }.filter { it !in visited }
        unvisited.forEach { pos ->
            val distance = paths[current]!!.size + 1
            if (distance < (paths[pos]?.size ?: Int.MAX_VALUE)) {
                paths[pos] = paths[current]!! + pos
            }
        }
        unvisited -= current
        visited += current
        current = unvisited.sortedBy { paths[it]!!.size }.firstOrNull() ?: break
    }
    return paths.values.toList()
}

fun main() {
    solveInputs { input ->
        val route = parseRoute(input.drop(1).dropLast(1))
        println(route)
        val position = Pos(0, 0)
        facility[position] = Room()
        exploreRoute(position, route)

        val paths = bfs(position)
//        val longestPath = paths.maxBy { it.size }
//        for (length in 1..longestPath!!.size) {
//            drawFacility(longestPath.take(length))
//        }
//        paths.forEach {
//            drawFacility(it)
//        }
        //drawFacility()

        println("Part 1: ${paths.maxBy { it.size }!!.size - 1}")
        println("Part 2: ${paths.filter { it.size > 1000 }.size}")
    }
}

var frame = 0
fun drawFacility(longestPath: List<Pos>? = null) {
//    val minX = facility.keys.minBy { it.x }!!.x
//    val minY = facility.keys.minBy { it.y }!!.y
//    val maxX = facility.keys.maxBy { it.x }!!.x
//    val maxY = facility.keys.maxBy { it.y }!!.y
    val minX = -49
    val minY = -52
    val maxX = 50
    val maxY = 47
    val width = maxX - minX + 1
    val height = maxY - minY + 1
    val roomSize = 3
    val pixelScale = 4
    val image = BufferedImage((width * (roomSize - 1) + 1) * pixelScale, (height * (roomSize - 1) + 1) * pixelScale, BufferedImage.TYPE_INT_BGR)
    for (y in 0 until image.height) {
        for (x in 0 until image.width) {
            image.setRGB(x, y, Color(255, 255, 255).rgb)
        }
    }
    for (y in minY..maxY) {
        for (x in minX..maxX) {
            facility[Pos(x, y)]?.let {
                val pixelX = ((x - minX) * (roomSize - 1)) * pixelScale
                val pixelY = ((y - minY) * (roomSize - 1)) * pixelScale
                drawRoom(image, pixelX, pixelY, it, pixelScale)
            }
        }
    }
    longestPath?.let {
        drawPath(image, it, minX, minY, pixelScale)
    }
    ImageIO.write(image, "png", File("facility/${String.format("%08d", frame)}.png"))
    frame++
}

fun drawFilledRoom(image: BufferedImage, x: Int, y: Int, color: Int, pixelScale: Int) {
    for (xOffset in 0..2) {
        for (yOffset in 0..2) {
            drawPixel(image, x + (xOffset * pixelScale), y + (yOffset * pixelScale), color, pixelScale)
        }
    }
}

fun drawPath(image: BufferedImage, path: List<Pos>, minX: Int, minY: Int, pixelScale: Int) {
    val pathColor = Color(3,169,244).rgb
    for (pair in path.zipWithNext()) {
        val posA = pair.first
        val posB = pair.second
        drawPixel(image, ((posA.x - minX) * 2 + 1) * pixelScale, ((posA.y - minY) * 2 + 1) * pixelScale, pathColor, pixelScale)
        drawPixel(image, ((posB.x - minX) * 2 + 1) * pixelScale, ((posB.y - minY) * 2 + 1) * pixelScale, pathColor, pixelScale)
        drawPixel(image, ((posA.x - minX + posB.x - minX) + 1) * pixelScale, ((posA.y - minY + posB.y - minY) + 1) * pixelScale, pathColor, pixelScale)
    }
}

fun drawRoom(image: BufferedImage, x: Int, y: Int, room: Room, pixelScale: Int) {
    val wallColor = Color(33,33,33).rgb
    val doorColor = Color(158,158,158).rgb//Color(78,52,46).rgb
    val floorColor = Color(158,158,158).rgb
    drawFilledRoom(image, x, y, wallColor, pixelScale)
    drawPixel(image, x + pixelScale, y + pixelScale, floorColor, pixelScale)
    if (room.doors[NORTH] == YES) {
        drawPixel(image, x + pixelScale, y, doorColor, pixelScale)
    }
    if (room.doors[SOUTH] == YES) {
        drawPixel(image, x + pixelScale, y + 2 * pixelScale, doorColor, pixelScale)
    }
    if (room.doors[EAST] == YES) {
        drawPixel(image, x + 2 * pixelScale, y + pixelScale, doorColor, pixelScale)
    }
    if (room.doors[WEST] == YES) {
        drawPixel(image, x, y + pixelScale, doorColor, pixelScale)
    }
}

fun drawPixel(image: BufferedImage, x: Int, y: Int, color: Int, pixelScale: Int) {
    for (xOffset in 0 until pixelScale) {
        for (yOffset in 0 until pixelScale) {
            image.setRGB(x + xOffset, y + yOffset, color)
        }
    }
}
