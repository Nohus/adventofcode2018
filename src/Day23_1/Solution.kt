package Day23_1

import solveInputs
import kotlin.math.abs

data class Pos(val x: Int, val y: Int, val z: Int)

data class NanoBot(val position: Pos, val radius: Int)

fun manhattanDistance(a: Pos, b: Pos): Int {
    return abs(a.x - b.x) + abs(a.y - b.y) + abs(a.z - b.z)
}

fun Pos.isInRange(other: Pos, range: Int): Boolean {
    val distance = manhattanDistance(this, other)
    return distance <= range
}

fun main() {
    solveInputs { input ->
        val bots = input.lines().map {
            val position = it.substringAfter("<").substringBefore(">").split(",").map(String::toInt).let {
                Pos(it[0], it[1], it[2])
            }
            val radius = it.substringAfter("r=").toInt()
            NanoBot(position, radius)
        }
        println(bots)
        val strongest = bots.maxBy { it.radius }!!
        val valid = bots.filter { it.position.isInRange(strongest.position, strongest.radius) }.size
        valid
    }
}
