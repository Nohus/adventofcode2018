package Day22_1

import Day22_1.Type.*
import solveInputs

enum class Type {
    ROCKY, NARROW, WET
}

data class Cave(private val regions: MutableMap<Pos, Region>, val target: Pos, val depth: Int) {

    operator fun get(position: Pos): Region {
        return regions[position] ?: Region().also { regions[position] = it }
    }

    operator fun set(position: Pos, region: Region) {
        regions[position] = region
    }

    private fun getErosionLevel(position: Pos): Int {
        return this[position].erosionLevel ?: run {
            return ((getGeologicIndex(position) + depth) % 20183).also { this[position].erosionLevel = it }
        }
    }

    private fun getGeologicIndex(position: Pos): Int {
        return this[position].geologicIndex ?: run {
            return when {
                position == target -> return 0
                position.y == 0 -> return position.x * 16807
                position.x == 0 -> return position.y * 48271
                else -> getErosionLevel(Pos(position.x - 1, position.y)) * getErosionLevel(Pos(position.x, position.y - 1))
            }.also { this[position].geologicIndex = it }
        }
    }

    private fun getType(position: Pos): Type {
        return this[position].type ?: run {
            return when (getErosionLevel(position) % 3) {
                0 -> ROCKY
                1 -> WET
                2 -> NARROW
                else -> throw Exception()
            }.also { this[position].type = it }
        }
    }

    fun computeTypes() {
        for (x in 0..target.x) {
            for (y in 0..target.y) {
                val position = Pos(x, y)
                this[position].type = getType(position)
            }
        }
    }

}
data class Pos(val x: Int, val y: Int)
data class Region(var geologicIndex: Int? = null, var erosionLevel: Int? = null, var type: Type? = null)

fun printCave(cave: Cave) {
    for (y in 0..cave.target.y) {
        for (x in 0..cave.target.x) {
            val pos = Pos(x, y)
            when (cave[pos].type) {
                ROCKY -> print(".")
                NARROW -> print("|")
                WET -> print("=")
                null -> print("?")
            }
        }
        println("")
    }
}

fun getRisk(cave: Cave): Int {
    var risk = 0
    for (y in 0..cave.target.y) {
        for (x in 0..cave.target.x) {
            risk += when (cave[Pos(x, y)].type) {
                ROCKY -> 0
                WET -> 1
                NARROW -> 2
                null -> throw Exception()
            }
        }
    }
    return risk
}

fun main() {
    solveInputs { input ->
        val depth = input.lines().first().substringAfter(" ").toInt()
        val targetCoordinates = input.lines()[1].substringAfter(" ").split(",").map(String::toInt)
        val target = Pos(targetCoordinates[0], targetCoordinates[1])
        val cave = Cave(mutableMapOf(), target, depth)
        cave.computeTypes()
        printCave(cave)
        getRisk(cave)
    }
}
