package Day2_1

import solveInputs

fun main() {
    solveInputs { input ->
        var twos = 0
        var threes = 0
        input.lines().forEach { box ->
            val letters = box.chunked(1)
            var two = false
            var three = false
            for (letter in letters) {
                val count = letters.count { it == letter }
                if (count == 2) two = true
                if (count == 3) three = true
            }
            if (two) twos++
            if (three) threes++
        }
        "${twos * threes}"
    }
}
