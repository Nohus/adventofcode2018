package Day6_2

import solveInputs
import kotlin.math.abs

fun dist(x: Int, y: Int, x1: Int, y1: Int): Int {
    return abs(x - x1) + abs(y - y1)
}

fun main() {
    solveInputs { input ->
        val coordinates = input.lines().map { it.split(", ")[0].toInt() to it.split(", ")[1].toInt() }
        val width = coordinates.map { it.first }.max()!! + 10
        val height = coordinates.map { it.second }.max()!! + 10
        var area = 0
        for (x in 0..width) {
            for (y in 0..height) {
                if (coordinates.map { dist(it.first, it.second, x, y) }.sum() < 10000) {
                    area++
                }
            }
        }
        "$area"
    }
}
