package Day18_2

import Day18_2.Acre.*
import solveInputs
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import kotlin.math.max
import kotlin.math.roundToInt

data class Pos(val x: Int, val y: Int)

enum class Acre {
    GROUND, TREES, LUMBERYARD
}

class Area {

    private val array = Array(width) { Array(height) { GROUND } }

    operator fun get(position: Pos): Acre {
        return array[position.x][position.y]
    }

    operator fun set(position: Pos, value: Acre) {
        array[position.x][position.y] = value
    }

}

const val width = 50 // 200 for extended video
const val height = 50 // 200
const val inputOffset = 0 // 75
var area = Area()
val history = mutableListOf(area)
var videoFrame = 0

fun drawArea() {
    val pixelScale = 2
    val image = BufferedImage(width * pixelScale, height * pixelScale, BufferedImage.TYPE_INT_BGR)
    for (y in 0 until height) {
        for (x in 0 until width) {
            val color = getColorWithAge(Pos(x, y))
            for (subX in 0 until pixelScale) {
                for (subY in 0 until pixelScale) {
                    image.setRGB((x * pixelScale) + subX, (y * pixelScale) + subY, color)
                }
            }
        }
    }
    ImageIO.write(image, "png", File("acres/${String.format("%08d", videoFrame)}.png"))
    videoFrame++
}

fun getAgedColor(color: Color, age: Int, factor: Double): Color {
    val factor = Math.pow(factor, age.toDouble())
    return Color(
        max(color.red * factor, 0.0).roundToInt(),
        max(color.green * factor, 0.0).roundToInt(),
        max(color.blue * factor, 0.0).roundToInt()
    )
}

fun getColorWithAge(position: Pos): Int {
    val acre = area[position]
    val age = position.getAge()
    return when (acre) {
        GROUND -> getAgedColor(Color(139,195,74), age, 0.9)
        TREES -> getAgedColor(Color(27,94,32), age, 0.7)
        LUMBERYARD -> getAgedColor(Color(121,85,72), age, 0.7)
    }.rgb
}

fun Pos.getAge(): Int {
    val acre = area[this]
    return history.reversed().takeWhile { it[this] == acre }.size
}

fun Pos.isInBounds(): Boolean {
    return x in 0..(width - 1) && y in 0..(height - 1)
}

fun Pos.getNeighbours(): List<Acre> {
    return listOf(
        Pos(x - 1, y - 1), Pos(x, y - 1), Pos(x + 1, y - 1),
        Pos(x - 1, y), Pos(x + 1, y),
        Pos(x - 1, y + 1), Pos(x, y + 1), Pos(x + 1, y + 1)
    ).filter { it.isInBounds() }.map { area[it] }
}

fun Pos.getNextGeneration(): Acre {
    val acre = area[this]
    return when (acre) {
        GROUND -> if (getNeighbours().count { it == TREES } >= 3) TREES else acre
        TREES -> if (getNeighbours().count { it == LUMBERYARD } >= 3) LUMBERYARD else acre
        LUMBERYARD -> if (getNeighbours().let { it.any { it == LUMBERYARD } && it.any { it == TREES } }) LUMBERYARD else GROUND
    }
}

fun getAllPositions(): List<Pos> {
    val positions = mutableListOf<Pos>()
    for (x in 0 until width) {
        for (y in 0 until height) {
            positions += Pos(x, y)
        }
    }
    return positions
}

fun tickArea() {
    val newArea = Area()
    getAllPositions().forEach { pos -> newArea[pos] = pos.getNextGeneration() }
    area = newArea
}

fun Area.isIdentical(other: Area): Boolean {
    for (position in getAllPositions()) {
        if (this[position] != other[position]) {
            return false
        }
    }
    return true
}

fun main() {
    solveInputs { input ->
        input.lines().forEachIndexed { y, line ->
            line.forEachIndexed { x, character ->
                area[Pos(x + inputOffset, y + inputOffset)] = when (character) {
                    '.' -> GROUND
                    '|' -> TREES
                    '#' -> LUMBERYARD
                    else -> throw Exception("Unknown input character: $character")
                }
            }
        }
        // Randomize map for extended video
//        getAllPositions().forEach {
//            area[it] = listOf(GROUND, GROUND, TREES, LUMBERYARD).random()
//        }

        val targetMinute = 1_000_000_000
        for (minute in 1..targetMinute) {
            tickArea()
            //drawArea()
            val repeatIndex = history.indexOfFirst { it.isIdentical(area) }
            if (repeatIndex > -1) {
                val cycleLength = minute - repeatIndex
                val minutesFromCycleStart = targetMinute - repeatIndex
                val remainingMinutes = minutesFromCycleStart % cycleLength
                repeat(remainingMinutes) {
                    tickArea()
                }
                break
            }
            history += area
        }
        val wood = getAllPositions().map { if (area[it] == TREES) 1 else 0 }.sum()
        val lumberyards = getAllPositions().map { if (area[it] == LUMBERYARD) 1 else 0 }.sum()
        wood * lumberyards
    }
}
