package Day17_1

import Day17_1.Direction.LEFT
import Day17_1.Direction.RIGHT
import solveInputs
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import javax.imageio.ImageIO
import kotlin.math.max
import kotlin.math.min

data class Pos(val x: Int, val y: Int)

enum class Direction {
    LEFT, RIGHT
}

abstract class Field
class Sand : Field()
class Clay : Field()
class Spring : Field()
class Water : Field()
class SettledWater : Field()

fun drawMap(drawReachability: Boolean) {
    val width = board.maxX - board.minX + 1
    val heightStart = min(max(lastSettleY - (width / 2), 0), board.maxY - width)
    val image = BufferedImage(width, width + 1/*board.maxY + 1*/, BufferedImage.TYPE_INT_BGR)

    for (x in board.minX..board.maxX) {
        for (y in heightStart..(heightStart + width)) {
            val field = board[Pos(x, y)]
            var color = when (field) {
                is Sand -> Color(255,235,59)
                is Clay -> Color(96,125,139)
                is Water -> Color(3,169,244)
                is Spring -> Color(26,35,126)
                is SettledWater -> Color(1,87,155)
                else -> throw Exception("Unknown field")
            }.rgb
            if (field is Sand && Pos(x, y) in waterReached) {
                color = Color(3,169,244).rgb
            }
            if (drawReachability) {
                if (Pos(x, y) in waterReached) {
                    color = Color(249,168,37).rgb
                }
            }
            image.setRGB(x - board.minX, y - heightStart, color)
        }
    }
    ImageIO.write(image, "png", File("map-$drawReachability.png"))
    Files.copy(File("map-$drawReachability.png").toPath(), File("water/${String.format("%08d", videoFrame)}.png").toPath(), StandardCopyOption.REPLACE_EXISTING)
    videoFrame++
}

class Board(val width: Int, val height: Int) {

    private val array = Array(width) { Array<Field>(height) { Sand() } }
    var minX = Int.MAX_VALUE
    private set
    var maxX = 0
    private set
    var minY = Int.MAX_VALUE
    private set
    var maxY = height
    private set

    operator fun get(position: Pos): Field {
        return array[position.x][position.y]
    }

    operator fun set(position: Pos, value: Field) {
        array[position.x][position.y] = value
    }

    fun calculateBounds() {
        var maxY = 0
        for (x in 0 until width) {
            for (y in 0 until height) {
                if (this[Pos(x, y)] !is Sand) {
                    if (x < minX) {
                        minX = x
                    }
                    if (x > maxX) {
                        maxX = x
                    }
                    if (y < minY) {
                        minY = y
                    }
                    if (y > maxY) {
                        maxY = y
                    }
                }
            }
        }
        this.maxY = maxY
        minX -= 10
        maxX += 10
    }

}

val board = Board(1500, 1660)
val waterReached = mutableSetOf<Pos>()
val activeWater = mutableSetOf<Pos>()
var lastSettleY = 0
var videoFrame = 1

fun getWaterTarget(position: Pos): Pos? {
    var target = position.copy(y = position.y + 1)
    if (board[target] is Sand) return target
    if (board[target] is Water) return null
    val direction = listOf(LEFT, RIGHT).random()
    target = if (direction == LEFT) {
        position.copy(x = position.x - 1)
    } else {
        position.copy(x = position.x + 1)
    }
    if (board[target] is Sand) return target
    return null
}

fun tickWater() {
    activeWater.filter { (board[it] is Water) }.forEach { waterPos ->
        getWaterTarget(waterPos)?.let { target ->
            val water = board[waterPos] as Water
            board[waterPos] = Sand()
            if (target.y <= board.height) {
                if (target.y > waterPos.y) {
                    board[target] = Water()
                } else {
                    board[target] = water
                }
                waterReached += target
                activeWater += target
                if (target.y == board.maxY) {
                    board[target] = Sand()
                }
            }
        }
    }
}

fun isStillWaterContainer(position: Pos, delta: Int): Boolean {
    val field = board[position]
    val belowField = board[position.copy(y = position.y + 1)]
    return when {
        field is Clay -> true
        belowField !is SettledWater && belowField !is Clay -> false
        else -> isStillWaterContainer(position.copy(x = position.x + delta), delta)
    }
}

fun fillWaterContainer(position: Pos, delta: Int) {
    when (board[position]) {
        is Clay -> return
        else -> {
            board[position] = SettledWater()
            waterReached += position
            fillWaterContainer(position.copy(x = position.x + delta), delta)
        }
    }
}

fun settleWater(): Boolean {
    var settled = false
    activeWater.forEach { waterPos ->
        if (board[waterPos] is Water) {
            if (isStillWaterContainer(waterPos, 1) && isStillWaterContainer(waterPos, -1)) {
                settled = true
                if (waterPos.y > lastSettleY) {
                    val amount = waterPos.y - lastSettleY
                    if (amount > 1) {
                        lastSettleY += 2
                    } else {
                        lastSettleY++
                    }
                }
                fillWaterContainer(waterPos, 1)
                fillWaterContainer(waterPos, -1)
            }
        }
    }
    activeWater.removeAll { board[it] is SettledWater }
    return settled
}

fun main() {
    solveInputs { input ->
        input.lines().forEach {
            val xDefinition: String
            val yDefinition: String
            if (it[0] == 'x') {
                xDefinition = it.substringAfter("x=").substringBefore(",").trim()
                yDefinition = it.substringAfter("y=").trim()
                val x = xDefinition.toInt()
                val yRangeDefinition = yDefinition.split("..").map(String::toInt)
                val yRange = (yRangeDefinition[0]..yRangeDefinition[1])
                for (y in yRange) {
                    board[Pos(x, y)] = Clay()
                }
            } else {
                yDefinition = it.substringAfter("y=").substringBefore(",").trim()
                xDefinition = it.substringAfter("x=").trim()
                val y = yDefinition.toInt()
                val xRangeDefinition = xDefinition.split("..").map(String::toInt)
                val xRange = (xRangeDefinition[0]..xRangeDefinition[1])
                for (x in xRange) {
                    board[Pos(x, y)] = Clay()
                }
            }
        }
        board.calculateBounds()
        board[Pos(500, board.minY - 1)] = Spring()
        waterReached += Pos(500, board.minY)
        activeWater += Pos(500, board.minY)
        var tick = 0
        var waterReachedChangeTick = 0
        var waterReachedSize = 0
        while (tick - waterReachedChangeTick < 1000) {
            board[Pos(500, board.minY)] = Water()
            tickWater()
            val settled = settleWater()
            if (settled) {
                drawMap(false)
            }
            if (waterReached.size != waterReachedSize) {
                waterReachedSize = waterReached.size
                waterReachedChangeTick = tick
            }
            tick++
        }
        repeat(90) {
            drawMap(false)
        }
        val atRest = waterReached.filter { board[it] is SettledWater }.size
        "Part 1: $waterReachedSize, Part 2: $atRest"
    }
}
